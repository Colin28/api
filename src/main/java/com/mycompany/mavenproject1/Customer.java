package com.mycompany.mavenproject1;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)

public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String addr;
    private String email;
    private String pw;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
/**
     * @return the addr
     */
    public String getAddr() {
        return addr;
    }
    /**
     * @param addr the name to set
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }
/**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
/**
     * @param email the name to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
/**
     * @return the pw
     */
    public String getPw() {
        return pw;
    }
/**
     * @param pw the name to set
     */
    public void setPw(String pw) {
        this.pw = pw;
    }
   
    @OneToMany(targetEntity=User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="customer")
    private List<User> user;

    public void setUsers(List<User> userlist) {
        this.user = userlist;
    }
    
    @XmlElementWrapper(name="users")
    @XmlElementRef()
    public List<User> getUsers() {
        return user;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", users=" + user + '}';
    }
    
    public static void main(String[] arges) throws PropertyException, JAXBException{
        StringWriter stringWriter = new StringWriter();

        JAXBContext ctx = JAXBContext.newInstance(Customer.class);
        Marshaller msh = ctx.createMarshaller();
    msh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        msh.marshal(new Customer(), System.out);
    }
}

