package com.mycompany.mavenproject1;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.namespace.QName;

@Entity
@Table
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({Transaction.class})


public class User implements Serializable {
    
    private int sortcode;
    private int balance;
    
    public User(){
        trans = new ArrayList<>();
          
         
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;
   
    
    @OneToMany(targetEntity=Transaction.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="user")
    private List<Transaction> trans;

    public void setTransactions(List<Transaction> employeelist) {
        this.trans = employeelist;
    }
    
    @XmlElementWrapper(name="transactions")
    @XmlElementRef()
    public List<Transaction> getTransactions() {
        return trans;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", transactions=" + trans + '}';
    }
   
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="customer_id")
    private Customer customer;

    @XmlTransient
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getSortcode() {
        return sortcode;
    }

    public void setSortcode(int sortcode) {
        this.sortcode = sortcode;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
    
    
    public static void main(String[] arges) throws PropertyException, JAXBException{
        StringWriter stringWriter = new StringWriter();

        JAXBContext ctx = JAXBContext.newInstance(User.class);
        Marshaller msh = ctx.createMarshaller();
    msh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        msh.marshal(new User(), System.out);
        
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("test-connection");
        EntityManager entitymanager = emfactory.createEntityManager();
        entitymanager.getTransaction().begin();

        Customer customer = new Customer();

        entitymanager.persist(customer);

        User trans1 = new User();
        User trans2 = new User();
        User trans3 = new User();
        User trans4 = new User();

        trans1.setCustomer(customer);
        trans2.setCustomer(customer);
        trans3.setCustomer(customer);
        trans4.setCustomer(customer);

        // entitymanager.persist(trans1);
        
        ArrayList<User> list = new ArrayList<>();
        list.add(trans1);
        list.add(trans2);
        list.add(trans3);
        list.add(trans4);
        
        
        customer.setUsers(list);
        entitymanager.persist(customer);
        
        entitymanager.getTransaction().commit();
        entitymanager.close();
        emfactory.close();
   
    }
}
